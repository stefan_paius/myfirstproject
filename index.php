<?php
require_once 'init.php';

//If no one is logged redirect to login.php
redirectIfNotLoggedIn();

// Getting user session data
$author = getLoggedUser();

// Condition that checks if there is post-data and there is an non--empty author post-data
if (isset($_POST['add-note']) && (!empty($author))) {
    $content = '';
    
//    var_dump($authorID);

    $content = mysqli_real_escape_string($dbconn, $_POST['content']);

    // Note input insertion query
    mysqli_query($dbconn, "INSERT INTO `notes` (`content`, `author_id`) VALUES ('$content', $author->id)")
            or die(mysqli_error($dbconn));
    echo "<h3><i>Your note has been added!</i></h3>";
}
// Displaying notes function
function getUserNotes($author_id) {
    include 'db-conn.php';
     
    $select = "SELECT * FROM `notes` INNER JOIN `users` ON notes.author_id = users.id WHERE author_id = $author_id";
    $result = mysqli_query($dbconn, $select);
     
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<br>Content: " . $row['content'] . "<br>" . ' Date: ' . $row['date'] . "<br>" . 'Author: ' . $row['fname'] . ' ' .$row['lname'] . "<br>";
        }
    } else {
        echo "No results";
    }
}

//$authorFullName =  ($author->fname . ' ' .$author->lname);


?>

<!DOCTYPE html>
<html>
    <head>
        <title> My first project </title>
    </head>

    <body>
        <h1> Important notes </h1>

        <h3> Oy,  <?php echo getLoggedUserFullName() ?> </h3>
        <a href="logout.php"><input type="submit" name="logout" value="Logout"></a>
        
    <legend><p>  - write down your notes and then click ADD - </p></legend>
    <form method="post" action="index.php">
        <textarea rows="10" name="content" cols="40"> </textarea>
        <br>
        <input type="submit" name="add-note" value="ADD" action="index.php"/>
    </form>

    <br>
    <hr>

    <div class="added-notes">
        <fieldset>
            <legend><h2> Your added-notes </h2></legend>
            <h4> <?php echo getUserNotes($author->id); ?> </h4>
        </fieldset>
    </div>
</body>


</html>
