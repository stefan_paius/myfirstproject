<?php

session_start();
include 'db-conn.php';

// Verify if user is logged in
function isLoggedIn() {
    return (isset($_SESSION['user']));
}

// Redirect function for non-logged users
function redirectIfNotLoggedIn() {
    if (!isLoggedIn()) {
        header("Location: login.php");
    }
}

// Redirect for logged users
function redirectifLogged() {
    if (isLoggedIn()) {
        header("Location: index.php");
    }
}

// Get user function 
function getLoggedUser() {
    return ($_SESSION['user']);
}

/**
 * get logged user full name
 * @return string
 */
function getLoggedUserFullName() {
    $loggedUser = getLoggedUser();
    return $loggedUser->fname . ' ' . $loggedUser->lname;
}

?>