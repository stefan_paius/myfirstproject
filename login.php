<?php
//Set up variables
$email = "";
$password = "";
$errorMess = "";
$num_rows = 0;

//Include DB & common used functions
require_once 'init.php';

// Store message variable
$error = '';

if (isset($_POST['login-btn'])) {
    if (empty($_POST['email']) || empty($_POST['password'])) {
        $error = "Invalid credentials";
    } else {
        // To protect MySQL injection for Security purpose
        $email = mysqli_real_escape_string($dbconn, $_POST['email']);
       
        // SQL query to fetch information of registerd users and finds user match.
        $result = mysqli_query($dbconn, "SELECT * FROM users WHERE email ='$email'");
        $rows = mysqli_num_rows($result);
        if ($rows == 1) {
            $user = mysqli_fetch_object($result);
            
//            var_dump(sha1($_POST['password']));
//            var_dump($user->password);
                       
            if (sha1($_POST['password']) == $user->password) {
            unset($user->password);
            
                // Initializing Session
                $_SESSION['user'] = $user;
                
                // Redirecting To Other Page
                header("Location: index.php"); 
            } else { 
                echo "Invalid credentials";
                header("Location: login.php");
            }
                
        } else {
            $error = "Invalid credentials";
        }
        mysqli_close($dbconn); // Closing Connection
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> My first project </title>
    </head>

    <body>

        <fieldset>
            <legend><h1> Welcome back! </h1></legend>
            <br>
            <form class="login-form" method="post">

                Email : <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                Password: <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>

                <input type="submit" name="login-btn" value="Log in" action="login.php">
                <br>
                <h3>Don't have an account? Click<a href="register.php"> here to register </a></h3>

            </form>
        </fieldset>




    </body>


</html>
