<?php
require_once 'init.php';

if (isset($_POST['register-btn'])) {

    //Credentials declaration
    $email = $_POST['email'];
    $password = $_POST['password'];
    $repPass = $_POST['repPass'];

    //Getting data from DB
    $getEmail = "SELECT email FROM `users` WHERE email='$email'";
    $query = mysqli_query($dbconn, $getEmail);
    $row = mysqli_fetch_array($query);


    //Setting conditions to check input email and password with DB email and password
    if (($email !== $row[0]) && ($password == $repPass)) {

        // Input email is different than DB email-> VALID -> Data input will be stored in DB
        $fname = mysqli_real_escape_string($dbconn, $_POST['fname']);
        $lname = mysqli_real_escape_string($dbconn, $_POST['lname']);
        $email = mysqli_real_escape_string($dbconn, $_POST['email']);

        //Password encryption
        $encPass = sha1($password);

        // Input data insertion query
        mysqli_query($dbconn, "INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`) VALUES ('NULL', '$fname', '$lname', '$email', '$encPass')")
                or die(mysqli_error($dbconn));
        header("Location: login.php");
    }

    // If condition for input email matching with DB existing email
    elseif (($email == $_POST['email']) && ($password == $repPass)) {
        echo "User already registered!<br>";
        exit();
    }
    //Condtition for mismatching input passwords 
    elseif (($email !== $row[0]) && ($password != $repPass)) {
        echo "Passwords do not match. Please try again! <br>";
        exit();
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> My first project </title>
        <link rel="stylesheet" type="text/css" href="assets/css/register.css">
    </head>

    <body>
        <fieldset>
            <legend><h1> Registration form </h1></legend>
            <br>
            <form class="register-form" method="post" action="register.php">

                First name: <input type="text" name="fname" required>
                Last name: <input type="text" name="lname" required>

                <!-- Email pattern defined by the model: "character@character.domain" -->
                Email: <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>

                <!-- Password pattern defined to contain at least one number and one uppercase and lowercase letter
                and a minimum of 6 character -->
                Password: <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
                Confirm password: <input type="password" name="repPass" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>

                <input type="submit" name="register-btn" value="Register">
                <br>
                <h3>Already registered? Click<a href="login.php"> here to login </a></h3>

            </form>
        </fieldset>



    </body>


</html>
